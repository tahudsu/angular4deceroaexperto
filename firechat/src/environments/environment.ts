// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAzznPfEGtBoStO70R-ZvqdPhUVMPDBHIo',
    authDomain: 'firechat-dff3d.firebaseapp.com',
    databaseURL: 'https://firechat-dff3d.firebaseio.com',
    projectId: 'firechat-dff3d',
    storageBucket: 'firechat-dff3d.appspot.com',
    messagingSenderId: '213739777794'
  }
};
