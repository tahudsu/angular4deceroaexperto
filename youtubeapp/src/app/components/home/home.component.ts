import { YoutubeService } from './../../services/youtube.service';
import { Component, OnInit } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {
  videos: any[] = [];
  videoSelected;
  constructor(public _youtube: YoutubeService) {
    this._youtube.getVideos().subscribe(videos => {
      this.videos = videos;
      console.log(this.videos);
    });
  }

  ngOnInit() {
  }

  verVideo(video:any) {
    this.videoSelected = video;
    console.log(this.videoSelected);
    $('#myLargeModalLabel').modal();
  }

  cerrarModal() {
    this.videoSelected = null;
    $('#myLargeModalLabel').modal('hide');
  }

  load() {
    this._youtube.getVideos().subscribe(videos => {
      this.videos.push.apply(this.videos, videos);
      console.log(this.videos);
    });
  }
}
