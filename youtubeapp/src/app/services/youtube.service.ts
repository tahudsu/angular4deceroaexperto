import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import 'rxjs/Rx';
@Injectable()
export class YoutubeService {
  private apiKey: string = `AIzaSyC_1RjCHH7MZjYV6N-MoMikiOQQtJiqvcA`;
  private url: string = `https://www.googleapis.com/youtube/v3`;
  private playlist: string = `UU8butISFwT-Wl7EV0hUK0BQ`;
  private nextPageToken: string = '';
  constructor(public _http: Http) { }

  getVideos() {
    const url = `${this.url}/playlistItems`;
    let params = new URLSearchParams();
    params.set('part', 'snippet');
    params.set('maxResults', '10');
    params.set('playlistId', this.playlist);
    params.set('key', this.apiKey);
    if (this.nextPageToken) {
      params.set('pageToken', this.nextPageToken);
    }
    return this._http.get(url, {search: params}).map((res) => {
      this.nextPageToken = res.json().nextPageToken;

      let videos:any[] = [];
      for (const video of res.json().items ) {
        const snippet = video.snippet;
        videos.push(snippet);
      }
      return videos;
    });
  }

}
