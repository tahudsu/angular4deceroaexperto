import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'filterSaveUrl'
})
export class FilterSaveUrlPipe implements PipeTransform {
  constructor(private sanitize: DomSanitizer) {}
  transform(value: string): any {
    const url = `https://www.youtube.com/embed/`;
    return this.sanitize.bypassSecurityTrustResourceUrl(url + value);
  }
}
