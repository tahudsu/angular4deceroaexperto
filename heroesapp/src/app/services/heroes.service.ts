import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Heroe } from '../interfaces/heroe.interface';
import 'rxjs/Rx';
@Injectable()
export class HeroesService {
  url = 'https://crudangular4.firebaseio.com/heroes.json';

  constructor(private http: Http) {

  }

  nuevoHeroe(heroe: Heroe) {
    const body = JSON.stringify(heroe);
    const headers = new Headers({
      'Content-Type' : 'application/json'
    });

    return this.http.post(this.url, body, { headers})
      .map(response => {
        console.log(response.json());
        return response.json();
      });
  }
}
