import { Component } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: 'body.component.html'
})
export class BodyComponent {
    visible:boolean = false;
    frase:any = {
        mensaje: `¿No es la vida cien veces demasiado breve para aburrirnos?`,
        autor: `Friederich Nietzsche`
    };

    personajes:string[] = [`Frodo`, `Sam`, `Merry`, `Pippin`, `Bilbo`];
}
