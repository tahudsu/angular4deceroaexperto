import { Component } from '@angular/core';
import { MapasService } from './services/mapas.service';
import { Marcador } from './interfaces/marcador.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  lat: number = 40.369115;
  lng: number = -3.6120528;
  zoom: number = 18;

  marcadorSel: any = null;

  constructor(public _ms: MapasService) {
    this._ms.cargarMarcadores();
  }

  clickMapa(evento) {
    console.log(evento);
    let nuevoMarcador: Marcador = {
      lat: evento.coords.lat,
      lng: evento.coords.lng,
      titulo: 'sin titulo',
      draggable: true
    };

    this._ms.insertarMarcador(nuevoMarcador);
  }

  clickMarcador(marcador: Marcador, index: number) {
    this.marcadorSel = marcador;
  }

  dragEndMarcador(marcador: Marcador, evento) {
    let lat = evento.coords.lat;
    let lng = evento.coords.lng;

    marcador.lat = lat;
    marcador.lng = lng;
    this._ms.guardarMarcadores();
  }

  borrarMarcador(index: number) {
    this._ms.borrarMarcador(index);
    this.marcadorSel = null;
  }
}
