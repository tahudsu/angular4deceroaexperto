import { Marcador } from './../interfaces/marcador.interface';
import { Injectable } from '@angular/core';

@Injectable()
export class MapasService {
  marcadores: Marcador[] = [];
  constructor() {
    let nuevoMarcador: Marcador = {
      lat: 40.369115,
      lng: -3.6120528,
      draggable: true,
      titulo: 'cerca de casa'
    };

    this.marcadores.push(nuevoMarcador);
   }

   insertarMarcador(marcador: Marcador) {
    console.warn(marcador);
    this.marcadores.push(marcador);
    console.error(this.marcadores);
    this.guardarMarcadores();
   }

   borrarMarcador(index: number) {
    this.marcadores.splice(index, 1);
    this.guardarMarcadores();
   }

   guardarMarcadores() {
     localStorage.setItem('marcadores', JSON.stringify(this.marcadores));
   }

   cargarMarcadores() {
     if (localStorage.getItem('marcadores')) {
      this.marcadores = JSON.parse(localStorage.getItem('marcadores'));
     } else {
       this.marcadores = [];
     }
   }
}
